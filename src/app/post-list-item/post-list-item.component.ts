import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() title: string;
  @Input() content: string;
  loveIts: number = 0;
  @Input() creationDate: Date;

  constructor() { }

  ngOnInit() {
  }

  onLoveIt(){
    this.loveIts++;
  }

  onDontLoveIt(){
    this.loveIts--;
  }

  //Cette méthode retourne la couleur du texte en fonction de la valeur de loveIts
  getColor(){
    if (this.loveIts < 0){
      return 'red';
    } else if (this.loveIts > 0){
      return 'green';
    } else {
      return 'black';
    }
  }

}
