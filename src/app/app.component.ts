import { Component } from '@angular/core';
import { reject } from 'q';
import { ResourceLoaderImpl } from '@angular/platform-browser-dynamic/src/resource_loader/resource_loader_impl';
import { getLocaleEraNames } from '@angular/common';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
  	styleUrls: ['./app.component.scss']
})
export class AppComponent {
	isAuthentified = false;
	
	lastUpdate = new Promise(
		(resolve,reject) => {
			const date = new Date();
			setTimeout(
				() => {
					resolve(date);
				},2000
			);
		}
	);

	posts = [
		{
			title: 'Mon premier post',
			content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Assumenda ad ea praesentium magnam corporis saepe vel laboriosam? Velit reprehenderit ea quidem, ducimus explicabo quaerat, molestias rem aliquam esse quisquam alias.',
			creationDate: Date()
		},
		{
			title: 'Mon deuxième post',
			content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Assumenda ad ea praesentium magnam corporis saepe vel laboriosam? Velit reprehenderit ea quidem, ducimus explicabo quaerat, molestias rem aliquam esse quisquam alias.',
			creationDate: Date()
		},
		{
			title: 'Encore un post',
			content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Assumenda ad ea praesentium magnam corporis saepe vel laboriosam? Velit reprehenderit ea quidem, ducimus explicabo quaerat, molestias rem aliquam esse quisquam alias.',
			creationDate: Date()
		}
	];
	/*
	appareils = [
		{
			name: 'Machine à laver',
			status: 'éteint'
		},
		{
			name: 'Télévision',
			status: 'éteint'
		},
		{
			name: 'Ordinateur',
			status: 'allumé'
		},
	];*/

  	constructor(){
    	setTimeout(
     		() => {
        		this.isAuthentified = true;
      		}, 4000
    	);
	}
	
	onAllumer(){
		console.log('On allume tout !');
	}
}